import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'sabpaisa Ionic integration',
  webDir: 'build',
  bundledWebRuntime: false
};

export default config;
