//
//  Sabpaisa.swift
//  App
//
//  Created by Lokesh D on 05/02/23.
//

import Foundation
import Capacitor
import SabPaisa_IOS_Sdk

@objc(Sabpaisa)
public class Sabpaisa: CAPPlugin {
    
    var initUrl="https://sdkstaging.sabpaisa.in/SabPaisa/sabPaisaInit?v=1"
    var baseUrl="https://sdkstaging.sabpaisa.in"
    var transactionEnqUrl="https://stage-txnenquiry.sabpaisa.in"
    
    @objc func nativeSdkCall(_ call: CAPPluginCall) {
        let firstName = call.getString("firstName") ?? ""
        let lastName = call.getString("lastName") ?? ""
        let email = call.getString("email") ?? ""
        let number = call.getString("number") ?? ""
        let amount = call.getString("amount") ?? ""
        
        
        
        
        let bundle = Bundle(identifier: "com.eywa.ios.SabPaisa-IOS-Sdk")
        let storyboard = UIStoryboard(name: "Storyboard", bundle: bundle)
        
        
        let secKey="kaY9AIhuJZNvKGp2"
        let secInivialVector="YN2v8qQcU3rGfA1y"
        let transUserName="rajiv.moti_336"
        let transUserPassword="RIADA_SP336"
        let clientCode="TM001"
        
        DispatchQueue.main.async{
        
        let vc = storyboard.instantiateViewController(withIdentifier: "InitialLoadViewController_Identifier") as! InitialLoadViewController
            vc.sdkInitModel=SdkInitModel(firstName: firstName, lastName: lastName, secKey: secKey, secInivialVector: secInivialVector, transUserName: transUserName, transUserPassword: transUserPassword, clientCode: clientCode, amount: Float(amount)!,emailAddress: email,mobileNumber: number,isProd: false,baseUrl: self.baseUrl, initiUrl: self.initUrl,transactionEnquiryUrl: self.transactionEnqUrl)
        
        
        vc.callback =  { (response:TransactionResponse)  in
            print("---------------Final Response To USER------------")
            call.resolve(["value": response.status])
            vc.dismiss(animated: true)
        }
        
            self.bridge?.viewController?.present(vc, animated: true,completion: nil)
        }
    }
}
